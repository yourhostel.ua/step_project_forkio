import gulp from 'gulp';
import imagemin from 'gulp-imagemin';
import autoprefixer from 'autoprefixer';
import browsersync from 'browser-sync';
import cssnano from 'cssnano';
import concat from 'gulp-concat';
import jsminify from 'gulp-js-minify';
import postcss from 'gulp-postcss';
import uglify from 'gulp-uglify';
import clean from 'gulp-clean';
import gulpSass from 'gulp-sass';
import dartSass from 'sass';

const sass = gulpSass(dartSass);

gulp.task('cleanTask', () => (
    gulp.src('dist/*')
        .pipe(clean())
));

gulp.task('cssTask', () => (
    gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles.min.css'))
        .pipe(postcss([autoprefixer('last 10 version'), cssnano()]))
        .pipe(gulp.dest('dist/css/'))
));

gulp.task('jsTask', () => (
    gulp.src('src/js/*.js')
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(jsminify())
        .pipe(gulp.dest('dist/js/'))
));

gulp.task('imgTask', () => (
    gulp.src('src/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img/'))
));

gulp.task('browsersyncServer', (cb) => {
    browsersync.init({
        server: {
            baseDir: '.'
        }
    });
    cb();
});

gulp.task('browsersyncReload', (cb) => {
    browsersync.reload();
    cb();
})

gulp.task('watchTask', () => {
    gulp.watch('*.html', gulp.series('browsersyncReload'));
    gulp.watch(['src/js/*.js', 'src/scss/*.scss'], gulp.series('jsTask', 'cssTask', 'browsersyncReload'));
})

gulp.task('dev', gulp.series('browsersyncServer', 'watchTask', 'jsTask', 'cssTask'))
gulp.task('build', gulp.series('cleanTask', 'cssTask', 'jsTask', 'imgTask'))
