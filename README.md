Проект виконали:
Тищенко Сергій
Масло Олександр

Спільно був зібраний пакет Gulp.

Стилі проекту реалізовувалися за допомогою препроцесора SASS scss синтаксису. 
Для компіляції scss були використані пакети у складі gulp - sass для компіляції, concat для об'єднання та 
postcss - який у свою чергу містить два пакети autoprefixer для додавання префіксів для більш ранніх 10 
версій браузерів і cssnano - який менімізує css.
Також до складу пакета gulp входить пакет для мінімізації зображень - imagemin,
для роботи з файлами зі сриптами js - concat(для об'єднання), uglify(для мініфікації), jsminify(для прибирання прогалин), 
а також пакет для запуску сервера і перезавантаження браузера - browsersync

Тищенко Сергій працював над завданням для студента №1

Завдання для студента №1
 * Зверстати шапку сайту з верхнім меню (включаючи випадаюче меню при малій роздільній здатності екрана.
 * Зверстати секцію People Are Talking About Fork.

Масло Олександр працював над завданням для студента №2

 * Зверстати блок Revolutionary Editor. Кнопки треба зробити, щоб виглядали як тут справа вгорі 
(звідти ж за допомогою інспектора можна взяти всі SVG іконки і завантажити стилі, що використовуються на гітхабі).
 * Зверстати секцію Here is what you get.
 * Зверстати секцію Fork Subscription Pricing. 
У блоці з цінами третій елемент завжди буде "виділений" і буде більшим за інші (тобто не по кліку/ховеру, а статично).

